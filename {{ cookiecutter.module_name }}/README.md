{{ cookiecutter.package_name }}
{{ '=' * cookiecutter.package_name|length }}

The {{ cookiecutter.module_name }} module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-{{ cookiecutter.module_name }}/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-{{ cookiecutter.module_name }})

Installing
----------

See INSTALL
{% if not cookiecutter.prefix %}

Support
-------

If you encounter any problems with Tryton, please don't hesitate to ask
questions on the Tryton bug tracker, mailing list, wiki or IRC channel:

  http://bugs.tryton.org/
  http://groups.tryton.org/
  http://wiki.tryton.org/
  irc://irc.freenode.net/tryton
{% endif %}

License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
{%- if not cookiecutter.prefix %}


For more information please visit the Tryton web site:

  http://www.tryton.org/
{%- endif %}
